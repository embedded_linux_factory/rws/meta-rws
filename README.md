# meta-rws

This project is a yocto layer that aims at including the DUT-manager and its dependencies into your distro.

## Dependencies

- meta-openembedded/meta-oe layer :
  - URI: https://git.openembedded.org/meta-openembedded \
  - subdirectory: meta-oe \
  - branch: kirkstone

## Recipes

### DUT-manager
provide the dut-manager, a config file and a systemd unit that launch it at startup.

needs :
 - systemd
 - libsocketcan (provided by meta-openembedded/meta-oe)
 - a patched version of libusbrelay (provided by this layer)

### libusbrelay

Provide controls for usb relay using hid linux api.
This is a patched version as required by the dut-manager.


## Get started

Set this in your local.conf :
```conf
MACHINE = "raspberrypi3-64"
require conf/rws.conf
```

then build your preferred image.

## Supported machines

 - ### Raspberry Pi 3 Model B

`MACHINE = "raspberrypi3-64"`

#### BSP Layer
  - URI: https://git.yoctoproject.org/meta-raspberrypi
  - branch: kirkstone

#### Specific configuration

Already included, specified in `conf/machine/include/rws-raspberrypi3-64.inc`.

 - ### ODROID-XU4

`MACHINE = "odroid-xu4"`

#### BSP Layer
  - URI: https://github.com/akuster/meta-odroid.git
  - branch: kirkstone

#### Specific configuration

Already included, specified in `conf/machine/include/odroid-xu4.inc`.

## Known bugs

systemd reports rws service status as failed :
```log
Error while parsing main server configuration
```
